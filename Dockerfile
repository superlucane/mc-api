FROM python:3.8-alpine

COPY . /app
WORKDIR /app

RUN pip3 install --no-cache-dir -r  dependencies.txt

ENV HOST 0.0.0.0
EXPOSE 5000

CMD ["python3", "api.py"]
