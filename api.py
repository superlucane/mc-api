from flask import Flask
from flask_cors import CORS, cross_origin
from flask_restful import Resource, Api, reqparse, abort, marshal, fields
from mcrcon import MCRcon
import os


mc_host = os.getenv('MC_HOST') or 'host.docker.internal'
mc_pass = os.getenv('MC_PASS') or 'unknown'

# Initialize Flask
app = Flask(__name__)
api = Api(app)
cors = CORS(app)


class UserList(Resource):
    def __init__(self):
        self.reqparse = reqparse.RequestParser()

    def get(self):
        msg = ''
        users = []
        try:
            with MCRcon(mc_host, mc_pass) as mcr:
                resp = mcr.command("/list")
                part = resp.split(':')
                users = [x.strip() for x in part[1].split(',')]
                return{"users": users, "msg": resp}
        except:
            if(len(msg) == 0):
                abort(404)
        return{"users": [], "msg": msg}

@app.route("/")
def home():
    return{"version": 1.1}

# @app.route("/test")
# def test():
#     resp = "There are 3 of a max of 20 players online: Mefru, pippo, caio"
#     part = resp.split(':')
#     # users = part[1].split()
#     users = [x.strip() for x in part[1].split(',')]
#     return{"users": users, "msg": resp}

api.add_resource(UserList, "/userlist")

if __name__ == "__main__":
    app.run(host="0.0.0.0", port=5000)